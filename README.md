# README #


### What is this repository for? ###

* This repository is used to manage AYA
* Version 2.0

### How do I get set up? ###

* Work Flow

Checkout the master branch to get stared on development of AYA. This is the master branch and development should not be directly done on this branch.

To work on a new feature, create a branch off of master with the following naming format : feature/AYA-1-describe-the-feature-you-are-working-on

Checkout this branch into your local repository and begin your work!

* Code review

Once you are done working on this branch, commit and push this branch and create a pull request to have it merged with master. Once your code is viewed and approved by your peer, it shall be merged to master.